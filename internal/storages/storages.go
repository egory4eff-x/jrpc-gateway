package storages

import (
	"gitlab.com/egory4eff-x/jrpc-gateway/internal/db/adapter"
	"gitlab.com/egory4eff-x/jrpc-gateway/internal/infrastructure/cache"
	vstorage "gitlab.com/egory4eff-x/jrpc-gateway/internal/modules/auth/storage"
	ustorage "gitlab.com/egory4eff-x/jrpc-gateway/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
