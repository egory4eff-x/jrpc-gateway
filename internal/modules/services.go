package modules

import (
	"gitlab.com/egory4eff-x/jrpc-gateway/internal/infrastructure/component"
	aservice "gitlab.com/egory4eff-x/jrpc-gateway/internal/modules/auth/service"
	uservice "gitlab.com/egory4eff-x/jrpc-gateway/internal/modules/user/service"
	"gitlab.com/egory4eff-x/jrpc-gateway/internal/storages"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
